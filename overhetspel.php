<?php 
session_start();

include("login_php/connection.php");
	include("login_php/functions.php");
	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html>
  <title>Overhetspel</title>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="javescript/contactjs.js"></script>
<link rel="stylesheet" href="css/contactcss.css">
<style>
	
    /* resets */

/* resets */

html {
  height: 100%;
  width: 100%;
  background-color:white;
  overflow : auto;

}
body{
 
    margin: 0 ;
    padding: 0;
    font-family: sans-serif;
    background-size: cover;
    background-image: url(https://i.pinimg.com/originals/ef/a9/39/efa9396fe51653bc074f75d2252692d0.gif);
    background-attachment: fixed;
  }
/* main */
header {
    height: 300px;
    z-index: 10;
    margin-bottom: 15%;
}
.header-banner {
    background-color: #333;
    background-image: url('https://i.gifer.com/Fp4B.gif');
    background-position: center;
    background-repeat: no-repeat;
    background-size:cover;
   position: relative;
  background-attachment: fixed;
    width: 100%;
    height: 450px;
}

header h1 {
    background-color: rgba(18,72,120, 0.8);
    color: #fff;
    padding: 0 1rem;
    position: absolute;
    top: 9rem; 
    left: 0.1rem;
    font-size: 2vw;
    
}
header a{
  font-size: 1.6vw;

}


/* demo content */



.active {
  background-color: orange;
  color: white;
}

.logo{
      width: 20%;
      
      float: right;
    
}
article {
  margin-top: 15%;
  color: white;
}
#demo {
    box-sizing: border-box;
  background-color: #292F33;
  width: 150px;
    height: 50px;
    font-size: 1em;
    margin: left;
    text-align: center;
    color: white;
  }
 #datum{
  box-sizing: border-box;
  background-color: #3F729B;
  width: 150px;
    height: 50px;
    font-size: 1em;
    margin: left;
    text-align: center;
    color: white;
 }


 section{
   margin-bottom: 10%;
 }


/* STYLES SPECIFIC TO FOOTER  */
.footer {
  font-family: Arial, Helvetica, Sans-;
  width: 100%;
  position: relative;
  height: auto;
  background-color: #070617;
 
}
.footer .col {
  width: 190px;
  height: auto;
  float: left;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  padding: 0px 20px 20px 20px;
}
.footer .col h1 {
  margin: 0;
  padding: 0;
  font-family: inherit;
  font-size: 12px;
  line-height: 17px;
  padding: 20px 0px 5px 0px;
  color: rgba(255,255,255,0.2);
  font-weight: normal;
  text-transform: uppercase;
  letter-spacing: 0.250em;
}
.footer .col ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
.footer .col ul li {
  color: #999999;
  font-size: 14px;
  font-family: inherit;
  font-weight: bold;
  padding: 5px 0px 5px 0px;
  cursor: pointer;
  transition: .2s;
  -webkit-transition: .2s;
  -moz-transition: .2s;
}
address{
  color: #999999;
}
.social ul li {
  display: inline-block;
 
}

.footer .col ul li:hover {
  color: #ffffff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
}
.clearfix {
  clear: both;
}
.slider{
  width: 800px;
  height: 400px;
  background-image: url(https://wallpaperaccess.com/full/25698.jpg);
  margin: 50px auto;
  animation: slide 15s infinite;
  border:  black;
  border-style: double ;
   box-shadow: 5px 10px black;
  
  

}
@keyframes slide{
  25%{
    background: url(https://www.nawa3em.com/big/child-summer-5.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    
  }
  50%{
    background: url(https://fhras.net/wp-content/uploads/2021/01/img_5ff431a5123e9.jpg);
    background-repeat: no-repeat;
    background-size: cover;
  }
 
  75%{
    background: url(https://cdn-cms.f-static.com/uploads/1976248/2000_5c97015eae985.jpg);
    background-repeat: no-repeat;
    background-size: cover;
  }
  100%{
    background: url(https://nordindien-privatfahrer.com/wp-content/uploads/2019/12/DSC_0086-1030x690.jpg);
    background-repeat: no-repeat;
    background-size: cover;
  }
  
}
.newspaper {
  column-count: 3;
  column-gap: 40px;
  column-rule-style: dotted;
  font-size: 1.3vw;
}
.col a{
  color: #999999;
}
.col a:hover {
  color: #ffffff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
}
#one{
	margin-top: 20%;
	margin-bottom: 30%;
}
#twee{
	margin-top: 20%;
	margin-bottom: 30%;
}


.centertext {
    width: 900px;
    margin: 200px auto;
}

.centertext h1 {
    text-align: center;
    font-size: 80px;
    color: #fff;
    font-weight: bold;
    animation: textanimation 10s ease-in-out;
}

@keyframes textanimation {
    0% {
        letter-spacing: 20px;
    }
    100% {
        letter-spacing: 1px;
    }
}

.centertext p {
    text-align: center;
    font-size: 30px;
    color: #ff0000;
    font-weight: bold;
    animation: textanimation2 0.5s ease-in-out;
}

@keyframes textanimation2 {
    0% {
        transform: scale(0);
    }
    100% {
        transform: scale(1);
    }
}
.btn :hover {
  color: #fff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
      }
      .btn a{
        color: #999999;
      }
      video{
        width: 50%;
        margin-left: 30%;
        height: 20%;
        margin-top: 10%;
      }
      #namee{
      box-sizing: border-box;
      background-color: #fc3153;
      font-family: 'Luckiest Guy';
     
      width: 150px;
        height: 50px;
        font-size: 2vw;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee p{
      color: #484848;
  font-size: 910px;
  font-weight: bold;
  font-family: monospace;
  letter-spacing: 7px;
  cursor: pointer
     }
     #namee span{
      transition: .5s linear
}
#namee:hover span:nth-child(1){
  margin-right: 5px
}
#namee:hover span:nth-child(1):after{
  content: "'";
}
#namee:hover span:nth-child(2){
  margin-left: 30px
}
#namee:hover span{
  color: #fff;
  text-shadow: 0 0 10px #fff,
               0 0 20px #fff, 
               0 0 40px #fff;
}
@media only screen and (min-width: 1280px) {
  .contain {
    width: 1200px;
    margin: 0 auto;
  }
}
@media only screen and (max-width: 1139px) {
  .contain .social {
    width: 1000px;
    display: block;
  }
  .social h1 {
    margin: 0px;
  }


  
}
@media only screen and (max-width: 950px) {
  .footer .col {
    width: 33%;
  }
  .footer .col h1 {
    font-size: 14px;
  }
  .footer .col ul li {
    font-size: 13px;
  }
  
.nav-link{
  font-size: 3vw;
}
.slider{
  width: 300px;
  height: 200px;
  background-image: url(https://wallpaperaccess.com/full/25698.jpg);
  margin: 50px auto;
  margin-top: 40%;
  animation: slide 20s infinite;
  border:  black;
  border-style: double ;
   box-shadow: 5px 10px black;
  
  

}
iframe{
  width: 110%;
  height: 50%;
}
p{
  font-size: 3vw;
}

}
@media only screen and (max-width: 500px) {
    .footer .col {
      width: 50%;
    }
    .footer .col h1 {
      font-size: 14px;
    }
    .footer .col ul li {
      font-size: 13px;
    }
   
.nav-link{
  font-size: 3vw;
}
iframe{
  width: 100%;
  height: 50%;
}
.slider{
  margin-top: 45%;
}

p{
  font-size: 3vw;
}
}
@media only screen and (max-width: 340px) {
  .footer .col {
    width: 100%;
  }
}

</style>
<header>
  <div class="header-banner">
    <p id="datum"></p>
  <p id="demo"></p>
  <p id="namee">	Hello,<span><?php echo $user_data['user_name']; ?></span></p> 
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">   
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item active">
    <nav class="topnav" id="active">
    <a class="nav-link" href="#">Home</a> <span class="sr-only"></span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="index.php">Game</a>
    </li>
     <li class="nav-item">
    <a class="nav-link"  href="overhetspel.php">hetspel</a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="ontwekelaars.php">Ontwikkelaars</a>
    <li class="nav-item">
    <a class="nav-link" href="ContactUs.php">Contact</a>
	</li>
    <li class="nav-item">
	<button type="button" class="btn btn-danger"><a href="login.php">Login</a></button>
	</li>
    
    <a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo"></a>
    </ul>
    </div>
    </nav>
    
    </div>
    
    </header>
  </head>
  
  <body>
  <video autoplay loop >
          <source src="videos/Chomp vs. Captain Shrederator - BattleBots.mp4" type="video/mp4" />
        </video>
	  <div id="one">
    <div class="centertext">
							<h1>BotBrawl Alpha Trailer (BETA)</h1>
    </div>
              <iframe width="260" height="315" src="https://www.youtube.com/embed/UOFCVFQ1DMM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							
							<br>
	  </div>
	  <div id="twee">
    <div class="centertext">
                            <h1>BotBrawl Weapon Showcase (BETA)</h1>
    </div>
							<br>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/apmlGr58ovk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              <br>
	  </div>
    <div class="centertext">
              <p>
                Pas je wapens aan door onderdelen te kiezen en te ruilen voor betere.<br>
                 Er zijn meerdere mogelijke combinaties van Parts en Projectiles die de manier waarop je speelt fundamenteel veranderen.</p>
    </div>
                 <br>
                 
                
                 <a href="#"><img src="foto/brawlbot.jpg" alt="" />
                  <br>
                  <div class="centertext">
                 <p>Neem je Bot online om tegen andere spelers te strijden en test zowel je vaardigheden als bouwer als als vechter.</p>
                  </div>
                 <a href="#"><img src="foto/images.png" alt="" />
                 <div class="centertext">
                 <P>Over ons:
                   <br>

                  
                   Wij zijn een viermans team uit Nederland. In de afgelopen paar maanden is dit project gegroeid tot de huidige staat.</P>
							
                 </div>
							
			









  

							
							<br>

							
                            <br>
                            <br>
                            <br>
                            <br>
						</div>
					</section>
    
  </body>
<footer>
    <div class="footer">
        <div class="contain">
        <div class="col">
          <h1>copyright</h1>
          <ul>
            <li> &copy;2021 alle rechten voorbehouden door :</li>
            <li>Mohammad Omar Aldyab</li>
            <li><a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo" style="width: 90%; height: 90%;"></a></li>
          </ul>
        </div>
        <div class="col">
          <h1>Pagina's</h1>
          <ul>
            <li><a href="/index.html">Home</a></li>
            <li><a href="/html/Zonvakanties.html">Zonvakanties</a></li>
            <li><a href="/html/wintersport-Ski.html">wintersport-Ski</a></li>
            <li><a href="/html/Vliegtickets.html">Vliegtickets</a></li>
            <li><a href="/html/autoverhuur.html"> Autoverhuur</a></li>
            <li><a href="/html/gastenboek.html">Gastenboek</a></li>
            <li><a href="/html/contact.html">Contact</a></li>
          </ul>
        </div>
        <div class="col">
          <h1>Support</h1>
          <ul>
            <li>Contact us</li>
            <address>
              6039EH Roermond . Bredaweg 22193<br>
            </address>
          </ul>
        </div>
        <div class="col social">
          <h1>Social</h1>
          <ul>
            <li> <a href="https://www.facebook.com"><img src="foto/facebook.png" width="32" style="width: 32px;"></a></li>
            <li><a href="http://www.instagram.com"><img src="foto/instagram.png" width="32" style="width: 32px;"></a></li>
            <li><a href="https://twitter.com"><img src="foto/twitter.png" width="32" style="width: 32px;"></a></li>
            <li><a href="http://www.youtube.com"><img src="foto/youtube.png" width="32" style="width: 32px;"></a></li>
            
          
          </ul>
        </div>
      <div class="clearfix"></div>
      </div>
      </div>
</footer>
      <!-- END OF FOOTER -->
        
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      <script>(function datum() {
        var d = new Date();
      document.getElementById('datum').innerHTML = d.toDateString();
      })();
      (function demo() {
        var today=new Date();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        document.getElementById('demo').innerHTML=h+":"+m+":"+s;
      
        setTimeout(demo, 1000);
      
      })();
           
    
    </script>
</body>
</html>
