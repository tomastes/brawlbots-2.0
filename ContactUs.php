<?php 
session_start();

include("login_php/connection.php");
	include("login_php/functions.php");
	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html>
  <title>Contact us</title>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="javescript/contactjs.js"></script>
<link rel="stylesheet" href="css/contactcss.css">
<style>
	
    /* resets */

    html {
      height: 100%;
      width: 100%;
      background-color:white;
      overflow : auto;
    
    }
    body{
     
        margin: 0 ;
        padding: 0;
        font-family: sans-serif;
       
        background-size: cover;
        background-image: url(https://media.wired.com/photos/5932334052d99d6b984dd067/master/w_2560%2Cc_limit/robogames_16x9-3.jpg);
        background-attachment: fixed;
      }
    /* main */
    header {
        height: 300px;
        z-index: 10;
        margin-bottom: 15%;
    }
    .header-banner {
        background-color: #333;
        background-image: url(https://wallpapercave.com/wp/wp2757861.gif);
        background-position: center;
        background-repeat: no-repeat;
        background-size:cover;
       position: relative;
      background-attachment: fixed;
        width: 100%;
        height: 450px;
    }
    
    header h1 {
        background-color: rgba(18,72,120, 0.8);
        color: #fff;
        padding: 0 1rem;
        position: absolute;
        top: 9rem; 
        left: 0.1rem;
        font-size: 2vw;
        
    }
    header a{
      font-size: 1.6vw;
    
    }
    
    
    
    /* demo content */
    
    
    
    .active {
      background-color: orange;
      color: white;
    }
    
    .logo{
          width: 20%;
     
          float: right;
        
    }
    article {
      margin-top: 15%;
      color:#fff;
    }
    #demo {
        box-sizing: border-box;
      background-color: #292F33;
      width: 150px;
        height: 50px;
        font-size: 1em;
        margin: left;
        text-align: center;
        color: white;
      }
     #datum{
      box-sizing: border-box;
      background-color: #3F729B;
      width: 150px;
        height: 50px;
        font-size: 1em;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee{
      box-sizing: border-box;
      background-color: #fc3153;
      font-family: 'Luckiest Guy';
     
      width: 150px;
        height: 50px;
        font-size: 2vw;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee p{
      color: #484848;
  font-size: 910px;
  font-weight: bold;
  font-family: monospace;
  letter-spacing: 7px;
  cursor: pointer
     }
     #namee span{
      transition: .5s linear
}
#namee:hover span:nth-child(1){
  margin-right: 5px
}
#namee:hover span:nth-child(1):after{
  content: "'";
}
#namee:hover span:nth-child(2){
  margin-left: 30px
}
#namee:hover span{
  color: #fff;
  text-shadow: 0 0 10px #fff,
               0 0 20px #fff, 
               0 0 40px #fff;
}
     
     /* STYLES SPECIFIC TO FOOTER  */
    .footer {
      font-family: Arial, Helvetica, Sans-;
      width: 100%;
      position: relative;
      height: auto;
      background-color: #070617;
     
    }
    .footer .col {
      width: 190px;
      height: auto;
      float: left;
      box-sizing: border-box;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      padding: 0px 20px 20px 20px;
    }
    .footer .col h1 {
      margin: 0;
      padding: 0;
      font-family: inherit;
      font-size: 12px;
      line-height: 17px;
      padding: 20px 0px 5px 0px;
      color: rgba(255,255,255,0.2);
      font-weight: normal;
      text-transform: uppercase;
      letter-spacing: 0.250em;
    }
    .footer .col ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }
    .footer .col ul li {
      color: #999999;
      font-size: 14px;
      font-family: inherit;
      font-weight: bold;
      padding: 5px 0px 5px 0px;
      cursor: pointer;
      transition: .2s;
      -webkit-transition: .2s;
      -moz-transition: .2s;
    }
    address{
      color: #999999;
    }
    .social ul li {
      display: inline-block;
     
    }
    
    .footer .col ul li:hover {
      color: #ffffff;
      transition: .1s;
      -webkit-transition: .1s;
      -moz-transition: .1s;
    }
    form{width:340px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin:calc(50vh - 220px) auto;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    h2{margin:10px 0;
    padding-bottom:10px;
    width:180px;
    color:red;
    border-bottom:3px solid blue;}
    input{width:100%;
    padding:10px;
    box-sizing:border-box;
    background:none;
    outline:none
    ;resize:none;
    border:0;
    font-family:'Montserrat',sans-serif;
    transition:all .3s;
    border-bottom:2px solid green;}
    input:focus{border-bottom:2px solid palegreen;}
    p:before,p{content:attr(type);
    display:block;
    margin:28px 0 0;
    font-size:14px;
    color:orange}
    #button{
    margin-left: 30%;
    padding:8px 12px;
    
    font-family:'Montserrat',sans-serif;
    border:2px solid green;
    background:linear-gradient(to right, #f64f59, #c471c4, #12c2e9);color: #fff;
    cursor:pointer;
    transition:all .3s}
    #button:hover{background:linear-gradient(to right, rgba(255,0,0,0), rgba(255,0,0,1));
    color:#fff}
    .col a{
        color: #999999;
      }
      .col a:hover {
        color: #ffffff;
        transition: .1s;
        -webkit-transition: .1s;
        -moz-transition: .1s;
      }
      .btn :hover {
  color: #fff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
      }
      .btn a{
        color: #999999;
      }
 /*robots */
 .robot1:before {
  -webkit-animation: before 0.3s steps(1) infinite;
  -moz-animation: before 0.3s steps(1) infinite;
  -o-animation: before 0.3s steps(1) infinite;
  animation: before 0.3s steps(1) infinite;
}
.robot1 {
  position: relative;
  display: inline-block;
  width: 165px;height: 200px;
  left: 45%;
  margin-top: 2%;
 
}
.robot1:before, .robot1:after {
  position: absolute;
  content: '';
  display: block;
	width: 5px;height: 5px;
}
@-webkit-keyframes before { 0% { opacity: 1; } 50% { opacity: 0; }}
@-moz-keyframes before { 0% { opacity: 1; } 50% { opacity: 0; }}
@-o-keyframes before { 0% { opacity: 1; } 50% { opacity: 0; }}
@keyframes before { 0% { opacity: 1; } 50% { opacity: 0; }}

.robot1:after {
  opacity: 0;
  -webkit-animation: after 0.3s steps(1) infinite;
  -moz-animation: after 0.3s steps(1) infinite;
  -o-animation: after 0.3s steps(1) infinite;
  animation: after 0.3s steps(1) infinite;
}
@-webkit-keyframes after { 0% { opacity: 0; } 50% { opacity: 1; } }
@-moz-keyframes after { 0% { opacity: 0; } 50% { opacity: 1; } }

@keyframes after { 0% { opacity: 0; } 50% { opacity: 1; } }

.robot1:before {
  box-shadow:
    /* antenna */
    50px 0px 0 red,
    55px 0px 0 red,
    60px 0px 0 red,
    65px 0px 0 red,
    45px 5px 0 red,
    70px 5px 0 red,
    40px 10px 0 red,
    75px 10px 0 red,
    40px 15px 0 red,
    75px 15px 0 red,
    45px 20px 0 red,
    70px 20px 0 red,
    50px 25px 0 red,
    55px 25px 0 red,
    60px 25px 0 red,
    65px 25px 0 red,
    55px 30px 0 red,
    60px 30px 0 red,
    55px 35px 0 red,
    60px 35px 0 red,
    /* head */
    30px 40px 0 white,
    35px 40px 0 white,
    40px 40px 0 white,
    45px 40px 0 white,
    50px 40px 0 white,
    55px 40px 0 white,
    60px 40px 0 white,
    65px 40px 0 white,
    70px 40px 0 white,
    75px 40px 0 white,
    80px 40px 0 white,
    85px 40px 0 white, 
    25px 45px 0 white,
    90px 45px 0 white,
    25px 50px 0 white,
    90px 50px 0 white,
    20px 55px 0 white,
    25px 55px 0 white,
    40px 55px 0 white,
    75px 55px 0 white,
    90px 55px 0 white,
    95px 55px 0 white,
    15px 60px 0 white,
    20px 60px 0 white,
    25px 60px 0 white,
    40px 60px 0 white,
    75px 60px 0 white,
    90px 60px 0 white,
    95px 60px 0 white,
    100px 60px 0 white,
    20px 65px 0 white,
    25px 65px 0 white,
    40px 65px 0 white,
    75px 65px 0 white,
    90px 65px 0 white,
    95px 65px 0 white,
    25px 70px 0 white,
    90px 70px 0 white,
    25px 75px 0 white,
    90px 75px 0 white,
    30px 80px 0 white,
    35px 80px 0 white,
    40px 80px 0 white,
    45px 80px 0 white,
    50px 80px 0 white,
    55px 80px 0 white,
    60px 80px 0 white,
    65px 80px 0 white,
    70px 80px 0 white,
    75px 80px 0 white,
    80px 80px 0 white,
    85px 80px 0 white,
    /* neck */
    45px 85px 0 blue,
    50px 85px 0 blue,
    55px 85px 0 blue,
    60px 85px 0 blue,
    65px 85px 0 blue,
    70px 85px 0 blue,
    /* torso */
    35px 90px 0 black,
    40px 90px 0 black,
    45px 90px 0 black,
    50px 90px 0 black,
    55px 90px 0 black,
    60px 90px 0 black,
    65px 90px 0 black,
    70px 90px 0 black, 
    75px 90px 0 black,  
    80px 90px 0 black,  
    30px 95px 0 black,
    30px 100px 0 black,
    30px 105px 0 black,
    30px 110px 0 black,
    30px 115px 0 black,
    30px 120px 0 black,
    30px 125px 0 black,
    30px 130px 0 black,
    30px 135px 0 black,
    30px 140px 0 black,
    85px 95px 0 black,
    85px 100px 0 black,
    85px 105px 0 black,
    85px 110px 0 black,
    85px 115px 0 black,
    85px 120px 0 black,
    85px 125px 0 black,
    85px 130px 0 black,
    85px 135px 0 black,
    85px 140px 0 black,
    35px 145px 0 black,
    40px 145px 0 black,
    45px 145px 0 black,
    50px 145px 0 black,
    55px 145px 0 black,
    60px 145px 0 black,
    65px 145px 0 black,
    70px 145px 0 black, 
    75px 145px 0 black,  
    80px 145px 0 black,
    55px 100px 0 black,
    60px 100px 0 black,
    45px 105px 0 black,
    50px 105px 0 black,
    45px 110px 0 black,
    50px 110px 0 black,
    65px 105px 0 black,
    70px 105px 0 black,
    65px 110px 0 black,
    70px 110px 0 black,    
    40px 115px 0 black,
    40px 120px 0 black,
    55px 115px 0 black,
    55px 120px 0 black,
    60px 115px 0 black,
    60px 120px 0 black,
    75px 115px 0 black,
    75px 120px 0 black,    
    45px 125px 0 black,
    50px 125px 0 black,
    45px 130px 0 black,
    50px 130px 0 black,
    65px 125px 0 black,
    70px 125px 0 black,
    65px 130px 0 black,
    70px 130px 0 black, 
    55px 135px 0 black,
    60px 135px 0 black,
    
    /* left arm */
    15px 100px 0 black,
    20px 100px 0 black,
    25px 100px 0 black,
    10px 105px 0 black,
    15px 105px 0 black,
    20px 105px 0 black,
    25px 105px 0 black,
    5px 110px 0 black,
    10px 110px 0 black,
    15px 110px 0 black,
    5px 115px 0 black,
    10px 115px 0 black,
    15px 115px 0 black,
    5px 120px 0 black,
    10px 120px 0 black,
    15px 120px 0 black,
    10px 125px 0 black,
    5px 130px 0 black,
    10px 130px 0 black,
    15px 130px 0 black,
    0px 135px 0 black,
    5px 135px 0 black,
    15px 135px 0 black,
    20px 135px 0 black,
    5px 140px 0 black,
    15px 140px 0 black,
    /* right arm */
    90px 100px 0 black,
    95px 100px 0 black,
    100px 100px 0 black,
    90px 105px 0 black,
    95px 105px 0 black,
    100px 105px 0 black,
    105px 105px 0 black,
    100px 110px 0 black,
    105px 110px 0 black,
    110px 110px 0 black,
    100px 115px 0 black,
    105px 115px 0 black,
    110px 115px 0 black,
    100px 120px 0 black,
    105px 120px 0 black,
    110px 120px 0 black, 
    105px 125px 0 black,
    100px 130px 0 black,
    105px 130px 0 black,
    110px 130px 0 black,
    95px 135px 0 black,
    100px 135px 0 black,
    110px 135px 0 black,
    115px 135px 0 black,
    100px 140px 0 black,
    110px 140px 0 black,
    /* left leg */
    45px 150px 0 black,
    50px 150px 0 black,
    45px 155px 0 black,
    50px 155px 0 black,
    45px 160px 0 black,
    50px 160px 0 black,
    45px 165px 0 black,
    50px 165px 0 black,
    35px 170px 0 black,
    40px 170px 0 black,
    45px 170px 0 black,
    50px 170px 0 black,
    30px 175px 0 black,
    35px 175px 0 black,
    40px 175px 0 black,
    45px 175px 0 black,
    50px 175px 0 black,
    /* right leg */
    65px 150px 0 black,
    70px 150px 0 black,
    65px 155px 0 black,
    70px 155px 0 black,
    65px 160px 0 black,
    70px 160px 0 black,
    65px 165px 0 black,
    70px 165px 0 black,
    65px 170px 0 black,
    70px 170px 0 black,
    75px 170px 0 black,
    80px 170px 0 black,
    65px 175px 0 black,
    70px 175px 0 black,
    75px 175px 0 black,
    80px 175px 0 black,
    85px 175px 0 black
}
.robot1:after {
  box-shadow:
    /* antenna */
    /*fixed antenna
    50px 5px 0 black,
    55px 5px 0 black,
    60px 5px 0 black,
    65px 5px 0 black,
    45px 10px 0 black,
    70px 10px 0 black,  
    40px 15px 0 black,
    75px 15px 0 black,
    40px 20px 0 black,
    75px 20px 0 black,
    45px 25px 0 black,
    70px 25px 0 black,
    50px 30px 0 black,
    55px 30px 0 black,
    60px 30px 0 black,
    65px 30px 0 black,
    */
    /*animated antenna*/
    55px 5px 0 black,
    60px 5px 0 black,

    55px 10px 0 black,
    60px 10px 0 black,  
    55px 15px 0 black,
    60px 15px 0 black,
    55px 20px 0 black,
    60px 20px 0 black,
    55px 25px 0 black,
    60px 25px 0 black,
    55px 30px 0 black,
    60px 30px 0 black,
 
    55px 35px 0 black,
    60px 35px 0 black,
    55px 40px 0 black,
    60px 40px 0 black,
    
    /* head */
    30px 45px 0 black,
    35px 45px 0 black,
    40px 45px 0 black,
    45px 45px 0 black,
    50px 45px 0 black,
    55px 45px 0 black,
    60px 45px 0 black,
    65px 45px 0 black,
    70px 45px 0 black,
    75px 45px 0 black,
    80px 45px 0 black,
    85px 45px 0 black,
    25px 50px 0 black,
    90px 50px 0 black,
    25px 55px 0 black,
    90px 55px 0 black,
    20px 60px 0 black,
    25px 60px 0 black,
    40px 60px 0 black,
    75px 60px 0 black,
    90px 60px 0 black,
    95px 60px 0 black,
    15px 65px 0 black,
    20px 65px 0 black,
    25px 65px 0 black,
    40px 65px 0 black,
    75px 65px 0 black,
    90px 65px 0 black,
    95px 65px 0 black,
    100px 65px 0 black,
    20px 70px 0 black,
    25px 70px 0 black,
    40px 70px 0 black,
    75px 70px 0 black,
    90px 70px 0 black,
    95px 70px 0 black,
    25px 75px 0 black,
    90px 75px 0 black,
    25px 80px 0 black,
    90px 80px 0 black,
    30px 85px 0 black,
    35px 85px 0 black,
    40px 85px 0 black,
    45px 85px 0 black,
    50px 85px 0 black,
    55px 85px 0 black,
    60px 85px 0 black,
    65px 85px 0 black,
    70px 85px 0 black,
    75px 85px 0 black,
    80px 85px 0 black,
    85px 85px 0 black,
    /* neck */
    45px 90px 0 black,
    50px 90px 0 black,
    55px 90px 0 black,
    60px 90px 0 black,
    65px 90px 0 black,
    70px 90px 0 black,
    /* torso */
    35px 95px 0 black,
    40px 95px 0 black,
    45px 95px 0 black,
    50px 95px 0 black,
    55px 95px 0 black,
    60px 95px 0 black,
    65px 95px 0 black,
    70px 95px 0 black, 
    75px 95px 0 black,  
    80px 95px 0 black,
    30px 100px 0 black,
    30px 105px 0 black,
    30px 110px 0 black,
    30px 115px 0 black,
    30px 120px 0 black,
    30px 125px 0 black,
    30px 130px 0 black,
    30px 135px 0 black,
    30px 140px 0 black,
    30px 145px 0 black,
    85px 100px 0 black,
    85px 105px 0 black,
    85px 110px 0 black,
    85px 115px 0 black,
    85px 120px 0 black,
    85px 125px 0 black,
    85px 130px 0 black,
    85px 135px 0 black,
    85px 140px 0 black,
    85px 145px 0 black,
    35px 150px 0 black,
    40px 150px 0 black,
    45px 150px 0 black,
    50px 150px 0 black,
    55px 150px 0 black,
    60px 150px 0 black,
    65px 150px 0 black,
    70px 150px 0 black, 
    75px 150px 0 black,  
    80px 150px 0 black,
    55px 105px 0 black,
    60px 105px 0 black,
    45px 110px 0 black,
    50px 110px 0 black,
    45px 115px 0 black,
    50px 115px 0 black,
    65px 110px 0 black,
    70px 110px 0 black,
    65px 115px 0 black,
    70px 115px 0 black,    
    40px 120px 0 black,
    40px 125px 0 black,
    55px 120px 0 black,
    55px 125px 0 black,
    60px 120px 0 black,
    60px 125px 0 black,
    75px 120px 0 black,
    75px 125px 0 black,
    45px 130px 0 black,
    50px 130px 0 black,
    45px 135px 0 black,
    50px 135px 0 black,
    65px 130px 0 black,
    70px 130px 0 black,
    65px 135px 0 black,
    70px 135px 0 black, 
    55px 140px 0 black,
    60px 140px 0 black,
    /* left arm */
    15px 105px 0 white,
    20px 105px 0 white,
    25px 105px 0 white,
    10px 110px 0 white,
    15px 110px 0 white,
    20px 110px 0 white,
    25px 110px 0 white,
    5px 115px 0 white,
    10px 115px 0 white,
    15px 115px 0 white,
    5px 120px 0 white,
    10px 120px 0 white,
    15px 120px 0 white,
    5px 125px 0 white,
    10px 125px 0 white,
    15px 125px 0 white,
    10px 130px 0 white,
    5px 135px 0 white,
    10px 135px 0 white,
    15px 135px 0 white,
    0px 140px 0 white,
    5px 140px 0 white,
    15px 140px 0 white,
    20px 140px 0 white,
    5px 145px 0 white,
    15px 145px 0 white,
    /* right arm */
    90px 105px 0 white,
    95px 105px 0 white,
    100px 105px 0 white,
    90px 110px 0 white,
    95px 110px 0 white,
    100px 110px 0 white,
    105px 110px 0 white,
    100px 115px 0 white,
    105px 115px 0 white,
    110px 115px 0 white,
    100px 120px 0 white,
    105px 120px 0 white,
    110px 120px 0 white,
    100px 125px 0 white,
    105px 125px 0 white,
    110px 125px 0 white,
    105px 130px 0 white,
    100px 135px 0 white,
    105px 135px 0 white,
    110px 135px 0 white,
    95px 140px 0 white,
    100px 140px 0 white,
    110px 140px 0 white,
    115px 140px 0 white,
    100px 145px 0 white,
    110px 145px 0 white,
    /* left leg */
    45px 155px 0 white,
    50px 155px 0 white,
    40px 160px 0 white,
    45px 160px 0 white,
    45px 165px 0 white,
    50px 165px 0 white,
    35px 170px 0 white,
    40px 170px 0 white,
    45px 170px 0 white,
    50px 170px 0 white,
    30px 175px 0 white,
    35px 175px 0 white,
    40px 175px 0 white,
    45px 175px 0 white,
    50px 175px 0 white,
    /* right leg */
    65px 155px 0 white,
    70px 155px 0 white,
    70px 160px 0 white,
    75px 160px 0 white,
    65px 165px 0 white,
    70px 165px 0 white,
    65px 170px 0 white,
    70px 170px 0 white,
    75px 170px 0 white,
    80px 170px 0 white,
    65px 175px 0 white,
    70px 175px 0 white,
    75px 175px 0 white,
    80px 175px 0 white,
    85px 175px 0 white,
}



    @media only screen and (min-width: 1280px) {
      .contain {
        width: 1200px;
        margin: 0 auto;
      }
    }
    @media only screen and (max-width: 1139px) {
      .contain .social {
        width: 1000px;
        display: block;
      }
      .social h1 {
        margin: 0px;
      }
    
      
    }
    @media only screen and (max-width: 950px) {
      .footer .col {
        width: 33%;
      }
      .footer .col h1 {
        font-size: 14px;
      }
      .footer .col ul li {
        font-size: 13px;
      }
    
    .nav-link{
      font-size: 3vw;
    }
    .form{width:240px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin-top: 110%;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    }
    #w3review{
      width: 90%;
    }
    #button{
      
      margin-right: 25%;
    }
    iframe{
      width: 300px;
      float: left;
      height: 350px;
     
    }
    
    
    @media only screen and (max-width: 500px) {
        .footer .col {
          width: 50%;
        }
        .footer .col h1 {
          font-size: 14px;
        }
        .footer .col ul li {
          font-size: 13px;
        }
    
    .nav-link{
      font-size: 3vw;
    }
    .form{width:240px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin-top: 130%;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    }
    #w3review{
      width: 90%;
    }
    #button{
    
      margin-right: 25%;
    }
    iframe{
      width: 300px;
      float: left;
      height: 350px;
     
    }
    @media only screen and (max-width: 340px) {
      .footer .col {
        width: 100%;
      }
      
      
    }
</style>
<header>

  <div class="header-banner">
    <p id="datum"></p>
  <p id="demo"></p>
  <p id="namee">	Hello,<span><?php echo $user_data['user_name']; ?></span></p> 
  
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">   
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item active">
    <nav class="topnav" id="active">
    <a class="nav-link" href="#">Home</a> <span class="sr-only"></span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="index.php">Game</a>
    </li>
     <li class="nav-item">
    <a class="nav-link"  href="overhetspel.php">hetspel</a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="ontwekelaars.php">Ontwikkelaars</a>
    <li class="nav-item">
    <a class="nav-link" href="ContactUs.php">Contact</a>
	</li>
    <li class="nav-item">
	<button type="button" class="btn btn-danger"><a href="login.php">Login</a></button>
	</li>
  
	<br>  
    <a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo"></a>
    </ul>
    </div>
    </nav>
    
    </div>
    
    </header>
  </head>
  
  <body>
  <div class="robot1"></div>
  
      <form action="Contactsqlphp/form-process.php" method="POST">
            <div class="form-group">
                <label for="firstname">Firstname</label>
                <input type="text" name="firstname" id="firstname" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="lastname">Lastname</label>
                <input type="text" name="lastname" id="lastname" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text" name="phone" id="phone" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <input type="text" name="message" id="message" class="form-control" required>
            </div>
            <div class="form-group">
        <button class="btn btn-success" type="submit">Submit</button>
        <button class="btn btn-danger" type="reset">Reset</button>
    </div>
        </form>
  </br>
  
  <div class="mapouter"><div class="gmap_canvas"><iframe width="453" height="412" id="gmap_canvas" src="https://maps.google.com/maps?q=bredaweg,Roermond&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://grantorrent-es.com"></a><br><style>.mapouter{position:relative;text-align:right;height:412px;width:453px;}</style><a href="https://www.embedgooglemap.net"></a><style>.gmap_canvas {overflow:hidden;background:none!important;height:412px;width:453px;}</style></div></div>
    
  </body>
<footer>
    <div class="footer">
        <div class="contain">
        <div class="col">
          <h1>copyright</h1>
          <ul>
            <li> &copy;2021 alle rechten voorbehouden door :</li>
            <li>Mohammad Omar Aldyab</li>
            <li><a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo" style="width: 90%; height: 90%;"></a></li>
          </ul>
        </div>
        <div class="col">
          <h1>Pagina's</h1>
          <ul>
            <li><a href="/index.html">Home</a></li>
            <li><a href="/html/Zonvakanties.html">Zonvakanties</a></li>
            <li><a href="/html/wintersport-Ski.html">wintersport-Ski</a></li>
            <li><a href="/html/Vliegtickets.html">Vliegtickets</a></li>
            <li><a href="/html/autoverhuur.html"> Autoverhuur</a></li>
            <li><a href="/html/gastenboek.html">Gastenboek</a></li>
            <li><a href="/html/contact.html">Contact</a></li>
          </ul>
        </div>
        <div class="col">
          <h1>Support</h1>
          <ul>
            <li>Contact us</li>
            <address>
              6039EH Roermond . Bredaweg 22193<br>
            </address>
          </ul>
        </div>
        <div class="col social">
          <h1>Social</h1>
          <ul>
            <li> <a href="https://www.facebook.com"><img src="foto/facebook.png" width="32" style="width: 32px;"></a></li>
            <li><a href="http://www.instagram.com"><img src="foto/instagram.png" width="32" style="width: 32px;"></a></li>
            <li><a href="https://twitter.com"><img src="foto/twitter.png" width="32" style="width: 32px;"></a></li>
            <li><a href="http://www.youtube.com"><img src="foto/youtube.png" width="32" style="width: 32px;"></a></li>
            
          
          </ul>
        </div>
      <div class="clearfix"></div>
      </div>
      </div>
</footer>
      <!-- END OF FOOTER -->
        
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      <script>(function datum() {
        var d = new Date();
      document.getElementById('datum').innerHTML = d.toDateString();
      })();
      (function demo() {
        var today=new Date();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        document.getElementById('demo').innerHTML=h+":"+m+":"+s;
      
        setTimeout(demo, 1000);
      
      })();
           
    
    </script>
</body>
</html>
