<?php 
session_start();

include("login_php/connection.php");
	include("login_php/functions.php");
	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html>
  <title>Ontwikelaars</title>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="javescript/contactjs.js"></script>
<link rel="stylesheet" href="css/contactcss.css">
<style>
	
    /* resets */

    html {
      height: 100%;
      width: 100%;
      background-color:white;
      overflow : auto;
    
    }
 
.fnc-slider {
  overflow: hidden;
  box-sizing: border-box;
  position: relative;
  height: 100vh;
}
.fnc-slider *, .fnc-slider *:before, .fnc-slider *:after {
  box-sizing: border-box;
}
.fnc-slider__slides {
  position: relative;
  height: 100%;
  transition: -webkit-transform 1s 0.6666666667s;
  transition: transform 1s 0.6666666667s;
  transition: transform 1s 0.6666666667s, -webkit-transform 1s 0.6666666667s;
}
.fnc-slider .m--blend-dark .fnc-slide__inner {
  background-color: #8a8a;
}
.fnc-slider .m--blend-dark .fnc-slide__mask-inner {
  background-color: #575757;
}
.fnc-slider .m--navbg-dark {
  background-color: #575757;
}
.fnc-slider .m--blend-green .fnc-slide__inner {
  background-color: #6d9b98;
}
.fnc-slider .m--blend-green .fnc-slide__mask-inner {
  background-color: #42605E;
}
.fnc-slider .m--navbg-green {
  background-color: #42605E;
}
.fnc-slider .m--blend-red .fnc-slide__inner {
  background-color: #ea2329;
}
.fnc-slider .m--blend-red .fnc-slide__mask-inner {
  background-color: #990e13;
}
.fnc-slider .m--navbg-red {
  background-color: #990e13;
}
.fnc-slider .m--blend-blue .fnc-slide__inner {
  background-color: #59aecb;
}
.fnc-slider .m--blend-blue .fnc-slide__mask-inner {
  background-color: #2D7791;
}
.fnc-slider .m--navbg-blue {
  background-color: #2D7791;
}
.fnc-slide {
  overflow: hidden;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.fnc-slide.m--before-sliding {
  z-index: 2 !important;
  -webkit-transform: translate3d(100%, 0, 0);
          transform: translate3d(100%, 0, 0);
}
.fnc-slide.m--active-slide {
  z-index: 1;
  transition: -webkit-transform 1s 0.6666666667s ease-in-out;
  transition: transform 1s 0.6666666667s ease-in-out;
  transition: transform 1s 0.6666666667s ease-in-out, -webkit-transform 1s 0.6666666667s ease-in-out;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.fnc-slide__inner {
  position: relative;
  height: 100%;
  background-size: cover;
  background-position: center top;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.m--global-blending-active .fnc-slide__inner, .m--blend-bg-active .fnc-slide__inner {
  background-blend-mode: luminosity;
}
.m--before-sliding .fnc-slide__inner {
  -webkit-transform: translate3d(-100%, 0, 0);
          transform: translate3d(-100%, 0, 0);
}
.m--active-slide .fnc-slide__inner {
  transition: -webkit-transform 1s 0.6666666667s ease-in-out;
  transition: transform 1s 0.6666666667s ease-in-out;
  transition: transform 1s 0.6666666667s ease-in-out, -webkit-transform 1s 0.6666666667s ease-in-out;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.fnc-slide__mask {
  overflow: hidden;
  z-index: 1;
  position: absolute;
  right: 60%;
  top: 15%;
  width: 50.25vh;
  height: 67vh;
  margin-right: -90px;
  -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%, 0 0, 6vh 0, 6vh 61vh, 44vh 61vh, 44vh 6vh, 6vh 6vh);
          clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%, 0 0, 6vh 0, 6vh 61vh, 44vh 61vh, 44vh 6vh, 6vh 6vh);
  -webkit-transform-origin: 50% 0;
          transform-origin: 50% 0;
  transition-timing-function: ease-in-out;
}
.m--before-sliding .fnc-slide__mask {
  -webkit-transform: rotate(-10deg) translate3d(200px, 0, 0);
          transform: rotate(-10deg) translate3d(200px, 0, 0);
  opacity: 0;
}
.m--active-slide .fnc-slide__mask {
  transition: opacity 0.35s 1.2222222222s, -webkit-transform 0.7s 1.2222222222s;
  transition: transform 0.7s 1.2222222222s, opacity 0.35s 1.2222222222s;
  transition: transform 0.7s 1.2222222222s, opacity 0.35s 1.2222222222s, -webkit-transform 0.7s 1.2222222222s;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
  opacity: 1;
}
.m--previous-slide .fnc-slide__mask {
  transition: opacity 0.35s 0.6833333333s, -webkit-transform 0.7s 0.3333333333s;
  transition: transform 0.7s 0.3333333333s, opacity 0.35s 0.6833333333s;
  transition: transform 0.7s 0.3333333333s, opacity 0.35s 0.6833333333s, -webkit-transform 0.7s 0.3333333333s;
  -webkit-transform: rotate(10deg) translate3d(-200px, 0, 0);
          transform: rotate(10deg) translate3d(-200px, 0, 0);
  opacity: 0;
}
.fnc-slide__mask-inner {
  z-index: -1;
  position: absolute;
  left: 50%;
  top: 50%;
  width: 100vw;
  height: 100vh;
  margin-left: -50vw;
  margin-top: -50vh;
  background-size: cover;
  background-position: center center;
  background-blend-mode: luminosity;
  -webkit-transform-origin: 50% 16.5vh;
          transform-origin: 50% 16.5vh;
  transition-timing-function: ease-in-out;
}
.m--before-sliding .fnc-slide__mask-inner {
  -webkit-transform: translateY(0) rotate(10deg) translateX(-200px) translateZ(0);
          transform: translateY(0) rotate(10deg) translateX(-200px) translateZ(0);
}
.m--active-slide .fnc-slide__mask-inner {
  transition: -webkit-transform 0.7s 1.2222222222s;
  transition: transform 0.7s 1.2222222222s;
  transition: transform 0.7s 1.2222222222s, -webkit-transform 0.7s 1.2222222222s;
  -webkit-transform: translateX(0);
          transform: translateX(0);
}
.m--previous-slide .fnc-slide__mask-inner {
  transition: -webkit-transform 0.7s 0.3333333333s;
  transition: transform 0.7s 0.3333333333s;
  transition: transform 0.7s 0.3333333333s, -webkit-transform 0.7s 0.3333333333s;
  -webkit-transform: translateY(0) rotate(-10deg) translateX(200px) translateZ(0);
          transform: translateY(0) rotate(-10deg) translateX(200px) translateZ(0);
}
.fnc-slide__content {
  z-index: 2;
  position: absolute;
  left: 40%;
  top: 40%;
}
.fnc-slide__heading {
  margin-bottom: 10px;
  text-transform: uppercase;
}
.fnc-slide__heading-line {
  overflow: hidden;
  position: relative;
  padding-right: 20px;
  font-size: 100px;
  color: #fff;
  word-spacing: 10px;
}
.fnc-slide__heading-line:nth-child(2) {
  padding-left: 30px;
}
.m--before-sliding .fnc-slide__heading-line {
  -webkit-transform: translateY(100%);
          transform: translateY(100%);
}
.m--active-slide .fnc-slide__heading-line {
  transition: -webkit-transform 1.5s 1s;
  transition: transform 1.5s 1s;
  transition: transform 1.5s 1s, -webkit-transform 1.5s 1s;
  -webkit-transform: translateY(0);
          transform: translateY(0);
}
.m--previous-slide .fnc-slide__heading-line {
  transition: -webkit-transform 1.5s;
  transition: transform 1.5s;
  transition: transform 1.5s, -webkit-transform 1.5s;
  -webkit-transform: translateY(-100%);
          transform: translateY(-100%);
}
.fnc-slide__heading-line span {
  display: block;
}
.m--before-sliding .fnc-slide__heading-line span {
  -webkit-transform: translateY(-100%);
          transform: translateY(-100%);
}
.m--active-slide .fnc-slide__heading-line span {
  transition: -webkit-transform 1.5s 1s;
  transition: transform 1.5s 1s;
  transition: transform 1.5s 1s, -webkit-transform 1.5s 1s;
  -webkit-transform: translateY(0);
          transform: translateY(0);
}
.m--previous-slide .fnc-slide__heading-line span {
  transition: -webkit-transform 1.5s;
  transition: transform 1.5s;
  transition: transform 1.5s, -webkit-transform 1.5s;
  -webkit-transform: translateY(100%);
          transform: translateY(100%);
}
.fnc-slide__action-btn {
  position: relative;
  margin-left: 200px;
  padding: 5px 15px;
  font-size: 20px;
  line-height: 1;
  color: transparent;
  border: none;
  text-transform: uppercase;
  background: transparent;
  cursor: pointer;
  text-align: center;
  outline: none;
}
.fnc-slide__action-btn span {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-perspective: 1000px;
          perspective: 1000px;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
  transition: -webkit-transform 0.3s;
  transition: transform 0.3s;
  transition: transform 0.3s, -webkit-transform 0.3s;
  -webkit-transform-origin: 50% 0;
          transform-origin: 50% 0;
  line-height: 30px;
  color: #fff;
}
.fnc-slide__action-btn span:before {
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border: 2px solid #fff;
  border-top: none;
  border-bottom: none;
}
.fnc-slide__action-btn span:after {
  content: attr(data-text);
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  line-height: 30px;
  background: #1F2833;
  opacity: 0;
  -webkit-transform-origin: 50% 0;
          transform-origin: 50% 0;
  -webkit-transform: translateY(100%) rotateX(-90deg);
          transform: translateY(100%) rotateX(-90deg);
  transition: opacity 0.15s 0.15s;
}
.fnc-slide__action-btn:hover span {
  -webkit-transform: rotateX(90deg);
          transform: rotateX(90deg);
}
.fnc-slide__action-btn:hover span:after {
  opacity: 1;
  transition: opacity 0.15s;
}
.fnc-nav {
  z-index: 5;
  position: absolute;
  right: 0;
  bottom: 0;
}
.fnc-nav__bgs {
  z-index: -1;
  overflow: hidden;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
}
.fnc-nav__bg {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
}
.fnc-nav__bg.m--nav-bg-before {
  z-index: 2 !important;
  -webkit-transform: translateX(100%);
          transform: translateX(100%);
}
.fnc-nav__bg.m--active-nav-bg {
  z-index: 1;
  transition: -webkit-transform 1s 0.6666666667s;
  transition: transform 1s 0.6666666667s;
  transition: transform 1s 0.6666666667s, -webkit-transform 1s 0.6666666667s;
  -webkit-transform: translateX(0);
          transform: translateX(0);
}
.fnc-nav__controls {
  font-size: 0;
}
.fnc-nav__control {
  overflow: hidden;
  position: relative;
  display: inline-block;
  vertical-align: top;
  width: 100px;
  height: 50px;
  font-size: 14px;
  color: #fff;
  text-transform: uppercase;
  background: transparent;
  border: none;
  outline: none;
  cursor: pointer;
  transition: background-color 0.5s;
}
.fnc-nav__control.m--active-control {
  background: #1F2833;
}
.fnc-nav__control-progress {
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 2px;
  background: #fff;
  -webkit-transform-origin: 0 50%;
          transform-origin: 0 50%;
  -webkit-transform: scaleX(0);
          transform: scaleX(0);
  transition-timing-function: linear !important;
}
.m--with-autosliding .m--active-control .fnc-nav__control-progress {
  -webkit-transform: scaleX(1);
          transform: scaleX(1);
}
.m--prev-control .fnc-nav__control-progress {
  -webkit-transform: translateX(100%);
          transform: translateX(100%);
  transition: -webkit-transform 0.5s !important;
  transition: transform 0.5s !important;
  transition: transform 0.5s, -webkit-transform 0.5s !important;
}
.m--reset-progress .fnc-nav__control-progress {
  -webkit-transform: scaleX(0);
          transform: scaleX(0);
  transition: -webkit-transform 0s 0s !important;
  transition: transform 0s 0s !important;
  transition: transform 0s 0s, -webkit-transform 0s 0s !important;
}
.m--autosliding-blocked .fnc-nav__control-progress {
  transition: all 0s 0s !important;
  -webkit-transform: scaleX(0) !important;
          transform: scaleX(0) !important;
}

/* NOT PART OF COMMON SLIDER STYLES */
body {
  margin: 0;
}

.demo-cont {
  overflow: hidden;
  position: relative;
  height: 100vh;
  -webkit-perspective: 1500px;
          perspective: 1500px;
  background: #000;
}
.demo-cont__credits {
  box-sizing: border-box;
  overflow-y: auto;
  z-index: 1;
  position: absolute;
  right: 0;
  top: 0;
  width: 400px;
  height: 100%;
  padding: 20px 10px 30px;
  background: #303030;
  font-family: "Open Sans", Helvetica, Arial, sans-serif;
  color: #fff;
  text-align: center;
  transition: -webkit-transform 0.7s;
  transition: transform 0.7s;
  transition: transform 0.7s, -webkit-transform 0.7s;
  -webkit-transform: translate3d(100%, 0, 0) rotateY(-45deg);
          transform: translate3d(100%, 0, 0) rotateY(-45deg);
  will-change: transform;
}
.credits-active .demo-cont__credits {
  transition: -webkit-transform 0.7s 0.2333333333s;
  transition: transform 0.7s 0.2333333333s;
  transition: transform 0.7s 0.2333333333s, -webkit-transform 0.7s 0.2333333333s;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.demo-cont__credits *, .demo-cont__credits *:before, .demo-cont__credits *:after {
  box-sizing: border-box;
}
.demo-cont__credits-close {
  position: absolute;
  right: 20px;
  top: 20px;
  width: 28px;
  height: 28px;
  cursor: pointer;
}
.demo-cont__credits-close:before, .demo-cont__credits-close:after {
  content: "";
  position: absolute;
  left: 0;
  top: 50%;
  width: 100%;
  height: 2px;
  margin-top: -1px;
  background: #fff;
}
.demo-cont__credits-close:before {
  -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
}
.demo-cont__credits-close:after {
  -webkit-transform: rotate(-45deg);
          transform: rotate(-45deg);
}
.demo-cont__credits-heading {
  text-transform: uppercase;
  font-size: 40px;
  margin-bottom: 20px;
}
.demo-cont__credits-img {
  display: block;
  width: 60%;
  margin: 0 auto 30px;
  border-radius: 10px;
}
.demo-cont__credits-name {
  margin-bottom: 20px;
  font-size: 30px;
}
.demo-cont__credits-link {
  display: block;
  margin-bottom: 10px;
  font-size: 24px;
  color: #fff;
}
.demo-cont__credits-blend {
  font-size: 30px;
  margin-bottom: 10px;
}

.example-slider {
  z-index: 2;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
  transition: -webkit-transform 0.7s;
  transition: transform 0.7s;
  transition: transform 0.7s, -webkit-transform 0.7s;
}
.credits-active .example-slider {
  -webkit-transform: translate3d(-400px, 0, 0) rotateY(10deg) scale(0.9);
          transform: translate3d(-400px, 0, 0) rotateY(10deg) scale(0.9);
}
.example-slider .fnc-slide-1 .fnc-slide__inner,
.example-slider .fnc-slide-1 .fnc-slide__mask-inner {
  background-image: url("foto/my4.jpg");

  background-size: auto;
  background-repeat: no-repeat;
}
.example-slider .fnc-slide-2 .fnc-slide__inner,
.example-slider .fnc-slide-2 .fnc-slide__mask-inner {
  background-image: url("https://wallpapercave.com/wp/hhR6Ekl.jpg");
  
}
.example-slider .fnc-slide-3 .fnc-slide__inner,
.example-slider .fnc-slide-3 .fnc-slide__mask-inner {
  background-image: url("https://cdn.allwallpaper.in/wallpapers/1366x768/1314/japan-bmw-cars-vehicles-m6-1366x768-wallpaper.jpg");
}
.example-slider .fnc-slide-3 .fnc-slide__inner:before {
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: rgba(255, 255, 255, 0.1);
}
.example-slider .fnc-slide-4 .fnc-slide__inner,
.example-slider .fnc-slide-4 .fnc-slide__mask-inner {
  background-image: url("http://www.hdnicewallpapers.com/Walls/Big/Jaguar/Blue_Jaguar_F_Pace_2017_Car_HD_Wallpaper.jpg");
}
.example-slider .fnc-slide-4 .fnc-slide__inner:before {
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.2);
}
.example-slider .fnc-slide__heading,
.example-slider .fnc-slide__action-btn,
.example-slider .fnc-nav__control {
  font-family: "Open Sans", Helvetica, Arial, sans-serif;
}

/* COLORFUL SWITCH STYLES 
   ORIGINAL DEMO - https://codepen.io/suez/pen/WQjwOb */
.colorful-switch {
  position: relative;
  width: 180px;
  height: 77.1428571429px;
  margin: 0 auto;
  border-radius: 32.1428571429px;
  background: #cfcfcf;
}
.colorful-switch:before {
  content: "";
  z-index: -1;
  position: absolute;
  left: -5px;
  top: -5px;
  width: 190px;
  height: 87.1428571429px;
  border-radius: 37.1428571429px;
  background: #314239;
  transition: background-color 0.3s;
}
.colorful-switch:hover:before {
  background: #4C735F;
}
.colorful-switch__checkbox {
  z-index: -10;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
.colorful-switch__label {
  z-index: 1;
  overflow: hidden;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border-radius: 32.1428571429px;
  cursor: pointer;
}
.colorful-switch__bg {
  position: absolute;
  left: 0;
  top: 0;
  width: 540px;
  height: 100%;
  background: linear-gradient(90deg, #14DCD6 0, #10E7BD 180px, #EF9C29 360px, #E76339 100%);
  transition: -webkit-transform 0.5s;
  transition: transform 0.5s;
  transition: transform 0.5s, -webkit-transform 0.5s;
  -webkit-transform: translate3d(-360px, 0, 0);
          transform: translate3d(-360px, 0, 0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__bg {
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.colorful-switch__dot {
  position: absolute;
  left: 131.1428571429px;
  top: 50%;
  width: 5.1428571429px;
  height: 5.1428571429px;
  margin-left: -2.5714285714px;
  margin-top: -2.5714285714px;
  border-radius: 50%;
  background: #fff;
  transition: -webkit-transform 0.5s;
  transition: transform 0.5s;
  transition: transform 0.5s, -webkit-transform 0.5s;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__dot {
  -webkit-transform: translate3d(-80.3571428571px, 0, 0);
          transform: translate3d(-80.3571428571px, 0, 0);
}
.colorful-switch__on {
  position: absolute;
  left: 104.1428571429px;
  top: 22.5px;
  width: 19.2857142857px;
  height: 36px;
  transition: -webkit-transform 0.5s;
  transition: transform 0.5s;
  transition: transform 0.5s, -webkit-transform 0.5s;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__on {
  -webkit-transform: translate3d(-80.3571428571px, 0, 0);
          transform: translate3d(-80.3571428571px, 0, 0);
}
.colorful-switch__on__inner {
  position: absolute;
  width: 100%;
  height: 100%;
  transition: -webkit-transform 0.25s 0s cubic-bezier(0.52, -0.96, 0.51, 1.28);
  transition: transform 0.25s 0s cubic-bezier(0.52, -0.96, 0.51, 1.28);
  transition: transform 0.25s 0s cubic-bezier(0.52, -0.96, 0.51, 1.28), -webkit-transform 0.25s 0s cubic-bezier(0.52, -0.96, 0.51, 1.28);
  -webkit-transform-origin: 100% 50%;
          transform-origin: 100% 50%;
  -webkit-transform: rotate(45deg) scale(0) translateZ(0);
          transform: rotate(45deg) scale(0) translateZ(0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__on__inner {
  transition: -webkit-transform 0.25s 0.25s cubic-bezier(0.67, -0.16, 0.47, 1.61);
  transition: transform 0.25s 0.25s cubic-bezier(0.67, -0.16, 0.47, 1.61);
  transition: transform 0.25s 0.25s cubic-bezier(0.67, -0.16, 0.47, 1.61), -webkit-transform 0.25s 0.25s cubic-bezier(0.67, -0.16, 0.47, 1.61);
  -webkit-transform: rotate(45deg) scale(1) translateZ(0);
          transform: rotate(45deg) scale(1) translateZ(0);
}
.colorful-switch__on__inner:before, .colorful-switch__on__inner:after {
  content: "";
  position: absolute;
  border-radius: 2.5714285714px;
  background: #fff;
}
.colorful-switch__on__inner:before {
  left: 0;
  bottom: 0;
  width: 100%;
  height: 6.1428571429px;
}
.colorful-switch__on__inner:after {
  right: 0;
  top: 0;
  width: 6.1428571429px;
  height: 100%;
}
.colorful-switch__off {
  position: absolute;
  left: 131.1428571429px;
  top: 50%;
  width: 41.1428571429px;
  height: 41.1428571429px;
  margin-left: -20.5714285714px;
  margin-top: -20.5714285714px;
  transition: -webkit-transform 0.5s;
  transition: transform 0.5s;
  transition: transform 0.5s, -webkit-transform 0.5s;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__off {
  -webkit-transform: translate3d(-80.3571428571px, 0, 0);
          transform: translate3d(-80.3571428571px, 0, 0);
}
.colorful-switch__off:before, .colorful-switch__off:after {
  content: "";
  position: absolute;
  left: 0;
  top: 50%;
  width: 100%;
  height: 5.1428571429px;
  margin-top: -2.5714285714px;
  border-radius: 2.5714285714px;
  background: #fff;
  transition: -webkit-transform 0.25s 0.25s;
  transition: transform 0.25s 0.25s;
  transition: transform 0.25s 0.25s, -webkit-transform 0.25s 0.25s;
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__off:before, .colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__off:after {
  transition-delay: 0s;
}
.colorful-switch__off:before {
  -webkit-transform: rotate(45deg) scaleX(1) translateZ(0);
          transform: rotate(45deg) scaleX(1) translateZ(0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__off:before {
  -webkit-transform: rotate(45deg) scaleX(0) translateZ(0);
          transform: rotate(45deg) scaleX(0) translateZ(0);
}
.colorful-switch__off:after {
  transition-timing-function: cubic-bezier(0.67, -0.16, 0.47, 1.61);
  -webkit-transform: rotate(-45deg) scaleX(1) translateZ(0);
          transform: rotate(-45deg) scaleX(1) translateZ(0);
}
.colorful-switch__checkbox:checked ~ .colorful-switch__label .colorful-switch__off:after {
  transition-timing-function: ease;
  -webkit-transform: rotate(-45deg) scaleX(0) translateZ(0);
          transform: rotate(-45deg) scaleX(0) translateZ(0);
}

    /* main */
    header {
        height: 300px;
        z-index: 10;
        margin-bottom: 15%;
    }
    .header-banner {
        background-color: #333;
        background-image: url(https://i.pinimg.com/originals/5e/ba/2c/5eba2c9eda121d7252a04654a2d094c8.gif);
        background-position: center;
        background-repeat: no-repeat;
        background-size:cover;
       position: relative;
      background-attachment: fixed;
        width: 100%;
        height: 450px;
    }
    
    header h1 {
        background-color: rgba(18,72,120, 0.8);
        color: #fff;
        padding: 0 1rem;
        position: absolute;
        top: 9rem; 
        left: 0.1rem;
        font-size: 2vw;
        
    }
    header a{
      font-size: 1.6vw;
    
    }
    
    
    
    /* demo content */
    
    
    
    .active {
      background-color: orange;
      color: white;
    }
    
    .logo{
          width: 20%;
     
          float: right;
        
    }
    article {
      margin-top: 15%;
      color:#fff;
    }
    #demo {
        box-sizing: border-box;
      background-color: #292F33;
      width: 150px;
        height: 50px;
        font-size: 1em;
        margin: left;
        text-align: center;
        color: white;
      }
     #datum{
      box-sizing: border-box;
      background-color: #3F729B;
      width: 150px;
        height: 50px;
        font-size: 1em;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee{
      box-sizing: border-box;
      background-color: #fc3153;
      font-family: 'Luckiest Guy';
     
      width: 150px;
        height: 50px;
        font-size: 2vw;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee p{
      color: #484848;
  font-size: 910px;
  font-weight: bold;
  font-family: monospace;
  letter-spacing: 7px;
  cursor: pointer
     }
     #namee span{
      transition: .5s linear
}
#namee:hover span:nth-child(1){
  margin-right: 5px
}
#namee:hover span:nth-child(1):after{
  content: "'";
}
#namee:hover span:nth-child(2){
  margin-left: 30px
}
#namee:hover span{
  color: #fff;
  text-shadow: 0 0 10px #fff,
               0 0 20px #fff, 
               0 0 40px #fff;
}
     /* STYLES SPECIFIC TO FOOTER  */
    .footer {
      font-family: Arial, Helvetica, Sans-;
      width: 100%;
      position: relative;
      height: auto;
      background-color: #070617;
     
    }
    .footer .col {
      width: 190px;
      height: auto;
      float: left;
      box-sizing: border-box;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      padding: 0px 20px 20px 20px;
    }
    .footer .col h1 {
      margin: 0;
      padding: 0;
      font-family: inherit;
      font-size: 12px;
      line-height: 17px;
      padding: 20px 0px 5px 0px;
      color: rgba(255,255,255,0.2);
      font-weight: normal;
      text-transform: uppercase;
      letter-spacing: 0.250em;
    }
    .footer .col ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }
    .footer .col ul li {
      color: #999999;
      font-size: 14px;
      font-family: inherit;
      font-weight: bold;
      padding: 5px 0px 5px 0px;
      cursor: pointer;
      transition: .2s;
      -webkit-transition: .2s;
      -moz-transition: .2s;
    }
    address{
      color: #999999;
    }
    .social ul li {
      display: inline-block;
     
    }
    
    .footer .col ul li:hover {
      color: #ffffff;
      transition: .1s;
      -webkit-transition: .1s;
      -moz-transition: .1s;
    }
    form{width:340px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin:calc(50vh - 220px) auto;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    h2{margin:10px 0;
    padding-bottom:10px;
    width:180px;
    color:red;
    border-bottom:3px solid blue;}
    input{width:100%;
    padding:10px;
    box-sizing:border-box;
    background:none;
    outline:none
    ;resize:none;
    border:0;
    font-family:'Montserrat',sans-serif;
    transition:all .3s;
    border-bottom:2px solid green;}
    input:focus{border-bottom:2px solid palegreen;}
    p:before,p{content:attr(type);
    display:block;
    margin:28px 0 0;
    font-size:14px;
    color:orange}
    #button{
    margin-left: 30%;
    padding:8px 12px;
    
    font-family:'Montserrat',sans-serif;
    border:2px solid green;
    background:linear-gradient(to right, #f64f59, #c471c4, #12c2e9);color: #fff;
    cursor:pointer;
    transition:all .3s}
    #button:hover{background:linear-gradient(to right, rgba(255,0,0,0), rgba(255,0,0,1));
    color:#fff}
    .col a{
        color: #999999;
      }
      .col a:hover {
        color: #ffffff;
        transition: .1s;
        -webkit-transition: .1s;
        -moz-transition: .1s;
      }
      .btn :hover {
  color: #fff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
      }
      .btn a{
        color: #999999;
      }


    @media only screen and (min-width: 1280px) {
      .contain {
        width: 1200px;
        margin: 0 auto;
      }
    }
    @media only screen and (max-width: 1139px) {
      .contain .social {
        width: 1000px;
        display: block;
      }
      .social h1 {
        margin: 0px;
      }
    
      
    }
    @media only screen and (max-width: 950px) {
      .footer .col {
        width: 33%;
      }
      .footer .col h1 {
        font-size: 14px;
      }
      .footer .col ul li {
        font-size: 13px;
      }
    
    .nav-link{
      font-size: 3vw;
    }
    .form{width:240px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin-top: 110%;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    }
    #w3review{
      width: 90%;
    }
    #button{
      
      margin-right: 25%;
    }
    iframe{
      width: 300px;
      float: left;
      height: 350px;
     
    }
    
    
    @media only screen and (max-width: 500px) {
        .footer .col {
          width: 50%;
        }
        .footer .col h1 {
          font-size: 14px;
        }
        .footer .col ul li {
          font-size: 13px;
        }
    
    .nav-link{
      font-size: 3vw;
    }
    .form{width:240px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin-top: 130%;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    }
    #w3review{
      width: 90%;
    }
    #button{
    
      margin-right: 25%;
    }
    iframe{
      width: 300px;
      float: left;
      height: 350px;
     
    }
    @media only screen and (max-width: 340px) {
      .footer .col {
        width: 100%;
      }
      
      
    }
</style>
<header>
  <h1>moajd</h1>
  <div class="header-banner">
    <p id="datum"></p>
  <p id="demo"></p>
  <p id="namee">	Hello,<span><?php echo $user_data['user_name']; ?></span></p> 
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">   
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item active">
    <nav class="topnav" id="active">
    <a class="nav-link" href="#">Home</a> <span class="sr-only"></span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="index.php">Game</a>
    </li>
     <li class="nav-item">
    <a class="nav-link"  href="overhetspel.php">hetspel</a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="ontwekelaars.php">Ontwikkelaars</a>
    <li class="nav-item">
    <a class="nav-link" href="ContactUs.php">Contact</a>
	</li>
    <li class="nav-item">
	<button type="button" class="btn btn-danger"><a href="login.php">Login</a></button>
	</li>
    
    <a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo"></a>
    </ul>
    </div>
    </nav>
    
    </div>
    
    </header>
  </head>
  
  <body>
       <!-- 
Please note, that you can apply .m--global-blending-active to .fnc-slider 
to enable blend-mode for all background-images or apply .m--blend-bg-active
to some specific slides (.fnc-slide). It's disabled by default in this demo,
because it requires specific images, where more than 50% of bg is transparent or monotone
-->
<div class="demo-cont">
    <!-- slider start -->
    <div class="fnc-slider example-slider">
      <div class="fnc-slider__slides">
        <!-- slide start -->
        <div class="fnc-slide m--blend-green m--active-slide">
          <div class="fnc-slide__inner">
            <div class="fnc-slide__mask">
              <div class="fnc-slide__mask-inner"></div>
            </div>
            <div class="fnc-slide__content">
              <h2 class="fnc-slide__heading">
                <div class="fnc-slide__heading-line">
                  <span>Mo</span>
                </div>
                <div class="fnc-slide__heading-line">
                  <span></span>
                </div>
              </h2>
              <button type="button" class="fnc-slide__action-btn">
                Credits
                <span data-text="Credits">Credits</span>
              </button>
            </div>
          </div>
        </div>
        <!-- slide end -->
        <!-- slide start -->
        <div class="fnc-slide m--blend-dark">
          <div class="fnc-slide__inner">
            <div class="fnc-slide__mask">
              <div class="fnc-slide__mask-inner"></div>
            </div>
            <div class="fnc-slide__content">
              <h2 class="fnc-slide__heading">
                <div class="fnc-slide__heading-line">
                 
                </div>
                <div class="fnc-slide__heading-line">
                    <span>Ferrari</span>
                </div>
              </h2>
              <button type="button" class="fnc-slide__action-btn">
                Credits
                <span data-text="Credits">Credits</span>
              </button>
            </div>
          </div>
        </div>
        <!-- slide end -->
        <!-- slide start -->
        <div class="fnc-slide m--blend-red">
          <div class="fnc-slide__inner">
            <div class="fnc-slide__mask">
              <div class="fnc-slide__mask-inner"></div>
            </div>
            <div class="fnc-slide__content">
              <h2 class="fnc-slide__heading">
                <div class="fnc-slide__heading-line">
                 
                </div>
                <div class="fnc-slide__heading-line">
                  <span>BMW</span>
                </div>
              </h2>
              <button type="button" class="fnc-slide__action-btn">
                Credits
                <span data-text="Credits">Credits</span>
              </button>
            </div>
          </div>
        </div>
        <!-- slide end -->
        <!-- slide start -->
        <div class="fnc-slide m--blend-blue">
          <div class="fnc-slide__inner">
            <div class="fnc-slide__mask">
              <div class="fnc-slide__mask-inner"></div>
            </div>
            <div class="fnc-slide__content">
              <h2 class="fnc-slide__heading">
                <div class="fnc-slide__heading-line">
                  
                </div>
                <div class="fnc-slide__heading-line">
                  <span>Jaguar</span>
                </div>
              </h2>
              <button type="button" class="fnc-slide__action-btn">
                Credits
                <span data-text="Credits">Credits</span>
              </button>
            </div>
          </div>
        </div>
        <!-- slide end -->
      </div>
      <nav class="fnc-nav">
        <div class="fnc-nav__bgs">
          <div class="fnc-nav__bg m--navbg-green m--active-nav-bg"></div>
          <div class="fnc-nav__bg m--navbg-dark"></div>
          <div class="fnc-nav__bg m--navbg-red"></div>
          <div class="fnc-nav__bg m--navbg-blue"></div>
        </div>
        <div class="fnc-nav__controls">
          <button class="fnc-nav__control">
            Mohammad Aldyab
            <span class="fnc-nav__control-progress"></span>
          </button>
          <button class="fnc-nav__control">
            Ferrai
            <span class="fnc-nav__control-progress"></span>
          </button>
          <button class="fnc-nav__control">
            BMW
            <span class="fnc-nav__control-progress"></span>
          </button>
          <button class="fnc-nav__control">
            Thor
            <span class="fnc-nav__control-progress"></span>
          </button>
        </div>
      </nav>
    </div>
    <!-- slider end -->
  
  </div>
    
  </body>
<footer>
    <div class="footer">
        <div class="contain">
        <div class="col">
          <h1>copyright</h1>
          <ul>
            <li> &copy;2021 alle rechten voorbehouden door :</li>
            <li>Mohammad Omar Aldyab</li>
            <li><a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo" style="width: 90%; height: 90%;"></a></li>
          </ul>
        </div>
        <div class="col">
          <h1>Pagina's</h1>
          <ul>
            <li><a href="/index.html">Home</a></li>
            <li><a href="/html/Zonvakanties.html">Zonvakanties</a></li>
            <li><a href="/html/wintersport-Ski.html">wintersport-Ski</a></li>
            <li><a href="/html/Vliegtickets.html">Vliegtickets</a></li>
            <li><a href="/html/autoverhuur.html"> Autoverhuur</a></li>
            <li><a href="/html/gastenboek.html">Gastenboek</a></li>
            <li><a href="/html/contact.html">Contact</a></li>
          </ul>
        </div>
        <div class="col">
          <h1>Support</h1>
          <ul>
            <li>Contact us</li>
            <address>
              6039EH Roermond . Bredaweg 22193<br>
            </address>
          </ul>
        </div>
        <div class="col social">
          <h1>Social</h1>
          <ul>
            <li> <a href="https://www.facebook.com"><img src="foto/facebook.png" width="32" style="width: 32px;"></a></li>
            <li><a href="http://www.instagram.com"><img src="foto/instagram.png" width="32" style="width: 32px;"></a></li>
            <li><a href="https://twitter.com"><img src="foto/twitter.png" width="32" style="width: 32px;"></a></li>
            <li><a href="http://www.youtube.com"><img src="foto/youtube.png" width="32" style="width: 32px;"></a></li>
            
          
          </ul>
        </div>
      <div class="clearfix"></div>
      </div>
      </div>
</footer>
      <!-- END OF FOOTER -->
        
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      <script>(function datum() {
        var d = new Date();
      document.getElementById('datum').innerHTML = d.toDateString();
      })();
      (function demo() {
        var today=new Date();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        document.getElementById('demo').innerHTML=h+":"+m+":"+s;
      
        setTimeout(demo, 1000);
      
      })();
           
      (function() {

var $$ = function(selector, context) {
  var context = context || document;
  var elements = context.querySelectorAll(selector);
  return [].slice.call(elements);
};

function _fncSliderInit($slider, options) {
  var prefix = ".fnc-";

  var $slider = $slider;
  var $slidesCont = $slider.querySelector(prefix + "slider__slides");
  var $slides = $$(prefix + "slide", $slider);
  var $controls = $$(prefix + "nav__control", $slider);
  var $controlsBgs = $$(prefix + "nav__bg", $slider);
  var $progressAS = $$(prefix + "nav__control-progress", $slider);

  var numOfSlides = $slides.length;
  var curSlide = 1;
  var sliding = false;
  var slidingAT = +parseFloat(getComputedStyle($slidesCont)["transition-duration"]) * 1000;
  var slidingDelay = +parseFloat(getComputedStyle($slidesCont)["transition-delay"]) * 1000;

  var autoSlidingActive = false;
  var autoSlidingTO;
  var autoSlidingDelay = 5000; // default autosliding delay value
  var autoSlidingBlocked = false;

  var $activeSlide;
  var $activeControlsBg;
  var $prevControl;

  function setIDs() {
    $slides.forEach(function($slide, index) {
      $slide.classList.add("fnc-slide-" + (index + 1));
    });

    $controls.forEach(function($control, index) {
      $control.setAttribute("data-slide", index + 1);
      $control.classList.add("fnc-nav__control-" + (index + 1));
    });

    $controlsBgs.forEach(function($bg, index) {
      $bg.classList.add("fnc-nav__bg-" + (index + 1));
    });
  };

  setIDs();

  function afterSlidingHandler() {
    $slider.querySelector(".m--previous-slide").classList.remove("m--active-slide", "m--previous-slide");
    $slider.querySelector(".m--previous-nav-bg").classList.remove("m--active-nav-bg", "m--previous-nav-bg");

    $activeSlide.classList.remove("m--before-sliding");
    $activeControlsBg.classList.remove("m--nav-bg-before");
    $prevControl.classList.remove("m--prev-control");
    $prevControl.classList.add("m--reset-progress");
    var triggerLayout = $prevControl.offsetTop;
    $prevControl.classList.remove("m--reset-progress");

    sliding = false;
    var layoutTrigger = $slider.offsetTop;

    if (autoSlidingActive && !autoSlidingBlocked) {
      setAutoslidingTO();
    }
  };

  function performSliding(slideID) {
    if (sliding) return;
    sliding = true;
    window.clearTimeout(autoSlidingTO);
    curSlide = slideID;

    $prevControl = $slider.querySelector(".m--active-control");
    $prevControl.classList.remove("m--active-control");
    $prevControl.classList.add("m--prev-control");
    $slider.querySelector(prefix + "nav__control-" + slideID).classList.add("m--active-control");

    $activeSlide = $slider.querySelector(prefix + "slide-" + slideID);
    $activeControlsBg = $slider.querySelector(prefix + "nav__bg-" + slideID);

    $slider.querySelector(".m--active-slide").classList.add("m--previous-slide");
    $slider.querySelector(".m--active-nav-bg").classList.add("m--previous-nav-bg");

    $activeSlide.classList.add("m--before-sliding");
    $activeControlsBg.classList.add("m--nav-bg-before");

    var layoutTrigger = $activeSlide.offsetTop;

    $activeSlide.classList.add("m--active-slide");
    $activeControlsBg.classList.add("m--active-nav-bg");

    setTimeout(afterSlidingHandler, slidingAT + slidingDelay);
  };



  function controlClickHandler() {
    if (sliding) return;
    if (this.classList.contains("m--active-control")) return;
    if (options.blockASafterClick) {
      autoSlidingBlocked = true;
      $slider.classList.add("m--autosliding-blocked");
    }

    var slideID = +this.getAttribute("data-slide");

    performSliding(slideID);
  };

  $controls.forEach(function($control) {
    $control.addEventListener("click", controlClickHandler);
  });

  function setAutoslidingTO() {
    window.clearTimeout(autoSlidingTO);
    var delay = +options.autoSlidingDelay || autoSlidingDelay;
    curSlide++;
    if (curSlide > numOfSlides) curSlide = 1;

    autoSlidingTO = setTimeout(function() {
      performSliding(curSlide);
    }, delay);
  };

  if (options.autoSliding || +options.autoSlidingDelay > 0) {
    if (options.autoSliding === false) return;
    
    autoSlidingActive = true;
    setAutoslidingTO();
    
    $slider.classList.add("m--with-autosliding");
    var triggerLayout = $slider.offsetTop;
    
    var delay = +options.autoSlidingDelay || autoSlidingDelay;
    delay += slidingDelay + slidingAT;
    
    $progressAS.forEach(function($progress) {
      $progress.style.transition = "transform " + (delay / 1000) + "s";
    });
  }
  
  $slider.querySelector(".fnc-nav__control:first-child").classList.add("m--active-control");

};

var fncSlider = function(sliderSelector, options) {
  var $sliders = $$(sliderSelector);

  $sliders.forEach(function($slider) {
    _fncSliderInit($slider, options);
  });
};

window.fncSlider = fncSlider;
}());

/* not part of the slider scripts */

/* Slider initialization
options:
autoSliding - boolean
autoSlidingDelay - delay in ms. If audoSliding is on and no value provided, default value is 5000
blockASafterClick - boolean. If user clicked any sliding control, autosliding won't start again
*/
fncSlider(".example-slider", {autoSlidingDelay: 4000});

var $demoCont = document.querySelector(".demo-cont");

[].slice.call(document.querySelectorAll(".fnc-slide__action-btn")).forEach(function($btn) {
$btn.addEventListener("click", function() {
  $demoCont.classList.toggle("credits-active");
});
});

document.querySelector(".demo-cont__credits-close").addEventListener("click", function() {
$demoCont.classList.remove("credits-active");
});

document.querySelector(".js-activate-global-blending").addEventListener("click", function() {
document.querySelector(".example-slider").classList.toggle("m--global-blending-active");
});
    </script>
</body>
</html>
