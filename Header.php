<?php 
session_start();

include("login_php/connection.php");
	include("login_php/functions.php");
	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html>
  <title>Game</title>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="javescript/contactjs.js"></script>
<link rel="stylesheet" href="css/contactcss.css">
<style>
	
    /* resets */

    html {
      height: 100%;
      width: 100%;
      background-color:white;
      overflow : auto;
    
    }
    
    /* main */
    header {
        height: 300px;
        z-index: 10;
        margin-bottom: 15%;
    }
    .header-banner {
        background-color: #333;
        background-image: url(https://i.gifer.com/8AVn.gif);
        background-position: center;
        background-repeat: no-repeat;
        background-size:cover;
       position: relative;
      background-attachment: fixed;
        width: 100%;
        height: 450px;
    }
    
    header h1 {
        background-color: rgba(18,72,120, 0.8);
        color: #fff;
        padding: 0 1rem;
        position: absolute;
        top: 9rem; 
        left: 0.1rem;
        font-size: 2vw;
        
    }
    header a{
      font-size: 1.6vw;
    
    }
    
    
    /* demo content */
    
    
    
    .active {
      background-color: orange;
      color: white;
    }
    
    .logo{
          width: 20%;
     
          float: right;
        
    }
    article {
      margin-top: 15%;
      color:#fff;
    }
    #demo {
        box-sizing: border-box;
      background-color: #292F33;
      width: 150px;
        height: 50px;
        font-size: 1em;
        margin: left;
        text-align: center;
        color: white;
      }
     #datum{
      box-sizing: border-box;
      background-color: #3F729B;
      width: 150px;
        height: 50px;
        font-size: 1em;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee{
      box-sizing: border-box;
      background-color: #fc3153;
      font-family: 'Luckiest Guy';
     
      width: 150px;
        height: 50px;
        font-size: 2vw;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee p{
      color: #484848;
  font-size: 910px;
  font-weight: bold;
  font-family: monospace;
  letter-spacing: 7px;
  cursor: pointer
     }
     #namee span{
      transition: .5s linear
}
#namee:hover span:nth-child(1){
  margin-right: 5px
}
#namee:hover span:nth-child(1):after{
  content: "'";
}
#namee:hover span:nth-child(2){
  margin-left: 30px
}
#namee:hover span{
  color: #fff;
  text-shadow: 0 0 10px #fff,
               0 0 20px #fff, 
               0 0 40px #fff;
}
     /* STYLES SPECIFIC TO FOOTER  */
    .footer {
      font-family: Arial, Helvetica, Sans-;
      width: 100%;
      position: relative;
      height: auto;
      background-color: #070617;
     
    }
    .footer .col {
      width: 190px;
      height: auto;
      float: left;
      box-sizing: border-box;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      padding: 0px 20px 20px 20px;
    }
    .footer .col h1 {
      margin: 0;
      padding: 0;
      font-family: inherit;
      font-size: 12px;
      line-height: 17px;
      padding: 20px 0px 5px 0px;
      color: rgba(255,255,255,0.2);
      font-weight: normal;
      text-transform: uppercase;
      letter-spacing: 0.250em;
    }
    .footer .col ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }
    .footer .col ul li {
      color: #999999;
      font-size: 14px;
      font-family: inherit;
      font-weight: bold;
      padding: 5px 0px 5px 0px;
      cursor: pointer;
      transition: .2s;
      -webkit-transition: .2s;
      -moz-transition: .2s;
    }
    address{
      color: #999999;
    }
    .social ul li {
      display: inline-block;
     
    }
    
    .footer .col ul li:hover {
      color: #ffffff;
      transition: .1s;
      -webkit-transition: .1s;
      -moz-transition: .1s;
    }
  
    @media only screen and (min-width: 1280px) {
      .contain {
        width: 1200px;
        margin: 0 auto;
      }
    }
    @media only screen and (max-width: 1139px) {
      .contain .social {
        width: 1000px;
        display: block;
      }
      .social h1 {
        margin: 0px;
      }
    
      
    }
    @media only screen and (max-width: 950px) {
      .footer .col {
        width: 33%;
      }
      .footer .col h1 {
        font-size: 14px;
      }
      .footer .col ul li {
        font-size: 13px;
      }
    
    .nav-link{
      font-size: 3vw;
    }
    .form{width:240px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin-top: 110%;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    }
    #w3review{
      width: 90%;
    }
    #button{
      
      margin-right: 25%;
    }
    iframe{
      width: 300px;
      float: left;
      height: 350px;
     
    }
    
    
    @media only screen and (max-width: 500px) {
        .footer .col {
          width: 50%;
        }
        .footer .col h1 {
          font-size: 14px;
        }
        .footer .col ul li {
          font-size: 13px;
        }
    
    .nav-link{
      font-size: 3vw;
    }
    .form{width:240px;
    height:640px;
    background:#e6e6e6;
    border-radius:8px;
    box-shadow:0 0 40px -10px #000;
    margin-top: 130%;
    padding:20px 30px;
    max-width:calc(100vw - 40px);
    box-sizing:border-box;
    font-family:'Montserrat',sans-serif;
    position:relative}
    }
    #w3review{
      width: 90%;
    }
    #button{
    
      margin-right: 25%;
    }
    iframe{
      width: 300px;
      float: left;
      height: 350px;
     
    }
    @media only screen and (max-width: 340px) {
      .footer .col {
        width: 100%;
      }
      
      
    }
</style>
<header>
  <div class="header-banner">
    <p id="datum"></p>
  <p id="demo"></p>
  <p id="namee">	Hello,<span><?php echo $user_data['user_name']; ?></span></p> 
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">   
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item active">
    <nav class="topnav" id="active">
    <a class="nav-link" href="#">Home</a> <span class="sr-only"></span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="index.php">Game</a>
    </li>
     <li class="nav-item">
    <a class="nav-link"  href="overhetspel.php">hetspel</a>
    </li>
    <li class="nav-item">
    <a class="nav-link"  href="ontwekelaars.php">Ontwikkelaars</a>
    <li class="nav-item">
    <a class="nav-link" href="ContactUs.php">Contact</a>
	</li>
    <li class="nav-item">
	<button type="button" class="btn btn-danger"><a href="login.php">Login</a></button>
	</li>
    
    <a><img src="https://fanart.tv/fanart/movies/9928/hdmovielogo/robots-51e0172cb4d16.png" class="logo"></a>
    </ul>
    </div>
    </nav>
    
    </div>
    
    </header>
  </head>
   <!-- END OF FOOTER -->
        
   <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      <script>(function datum() {
        var d = new Date();
      document.getElementById('datum').innerHTML = d.toDateString();
      })();
      (function demo() {
        var today=new Date();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        document.getElementById('demo').innerHTML=h+":"+m+":"+s;
      
        setTimeout(demo, 1000);
      
      })();
           
    
    </script>