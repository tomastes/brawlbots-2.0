<?php

include("Header.php");

?>
<style type="text/css">
.game_form{
    background-color:whitesmoke;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height:100%;

}
.game_form>form{
padding:2rem;
background-color:white;
width:100%;
display:flex;
justify-content:center;
align-items: center;
flex-direction: column;
}
.input_container{
display: flex;
width:100%;
justify-content: space-between;
align-items: center;
flex-direction: column;
}

.input_container>input{
    padding:1rem .5rem;
    border-top: none !important;
    border-right: none !important;
    border-left: none !important;
    border-bottom: 1px solid gray;
    margin-bottom:1rem;
    outline: none !important;
}
.start_game:hover{
    background-color:gray;
        color:lightblue;
        transition:.3s ease-in-out;
        transform:scale(1.03)
}
.start_game{
    font-size: 22px;
        font-family: fantasy;
        color:gray;
        font-weight: 900;
        text-transform:uppercase;
      padding:1.3rem 4rem;
      background-color: lightblue;
      border: none;
      outline: none;
      cursor: pointer;
      transition:.3s ease-in-out;

      box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px;
}
</style>
<div class="game_form">
<form action="game.php" method="post">
<!-- <p class="title">customize your game</p>
<label for="timer">game timmer</label>
<select name="timmerDuration">
<option value=30>30</option>
<option value=50>50</option>
<option value=60>60</option>
</select> <br>
<label for="level">choose your level</label>
<select name="level">
<option value="starter">starter</option>
<option value="intermidate">intermidate</option>
<option value="advanced">advanced</option>
</select> <br>
<label for="asset">choose your asset</label>
<select name="asset">
<option value="XRP"><img src="https://static.wikia.nocookie.net/cryptocurrency/images/2/23/Ripple.png/revision/latest?cb=20171231154632" /> </option>
<option value="DOGE">DOGE</option>
<option value="ADA">ADA</option>
</select> <br> -->
<div class="input_container">
<input required type="text" name="player1" id="player"  placeholder="player1 name"/> <br>
<input required type="text" name="player2" id="player"  placeholder="player2 name"/> <br>
</div>
<input class="start_game" type="submit" value="play" name="game_start" />
</form>

</div>